import Weather from '../assets/projects/weather.jpg';
import Task from '../assets/projects/task.jpg';
import Disney from '../assets/projects/disney.jpg';

export const data = [
    {
        id: 1,
        name: "Weather-App",
        image: Weather,
        github: "https://github.com/nadyab93/weather-app",
        live: "https://nadyab93.github.io/weather-app/",
    },
    {
        id: 2,
        name: "Task-List",
        image: Task,
        github: "https://github.com/nadyab93/Task-List",
        live: "https://nadyab93.github.io/Task-List/",
    },
    {
        id: 3,
        name: "Disneyplus-Clone",
        image: Disney,
        github: "https://github.com/nadyab93/disneyplus-clone",
        live: "https://disneyplus-clone-sable.vercel.app/",
    },
];
